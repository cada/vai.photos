# Archive

The archive resource represents the images within the system.

### Routes

The following tasks are available for Archive resources.

| Endpoint   | Verb | Description | Controller#Action |
|:-----------|:-----|:------------|:------------------|
| **[/v1/archives(.:format)](#GET-/v1/archives(.:format))** | GET | Retrieves all published resources. | v1/archives#index |
| **[/v1/archives(.:format)](#POST-/v1/archives(.:format))** | POST | Creates a new archive. | v1/archives#create |
| **[/v1/archives/:id(.:format)](#GET-/v1/archives/:id(.:format))** | GET | Returns an archive. | v1/archives#show |
| **[/v1/archives/:id(.:format)](#PUT-/v1/archives/:id(.:format))** | PUT | Updates an archive. | v1/archives#update |
| **[/v1/archives/:id(.:format)](#DELETE-/v1/archives/:id(.:format))** | DELETE | Deletes an archive. | v1/archives#destroy |


### Sample JSON respresentation

Here is a JSON representation of Archive.

```json
{
  "id": "integer",
  "title": "string",
  "author": "string",
  "description": "string",
  "published": "boolean",
  "metadata": "json", 
  "created_at": "datetime",
  "file_url": "string"
}
```
### Properties

| Property | Type | Required | Default | Description |
|:---------|:-----|:---------|:--------|:------------|
| **id**   | Integer | True | Autoincremented | Unique identifier for the archive. Automatically generated. |
| **title** | String | False | None | The title of the archive. |
| **author** | String | False | None | A collection of author resources that represent the authors of the archive. |
| **description** | String | False | None | Descriptive text for the archive, including an abstract. |
| **published** | Boolean | False | True | Flag archive as published or not. Only published archives are visibles. |
| **metadata** | Hash | False | None | Metadata from image file. |
| **created_at** | DateTime | False | Time.now | The date and time the archive record was last modified. |


## GET /v1/archives(.:format)

Retrieves all published archives.

### URL Sample

```
/v1/archives.json
```

### URL Params

None

### Data params

None

### Examples

##### Request

```
  curl -X GET 'http://localhost:3000/v1/archives.json'
```

##### Response

```json
[
  {
    "id": 42,
    "title": "Don't Panic",
    "file_url": "https://vaiapi.s3.region.amazonaws.com/archive/42/image/original.jpg"
  },
  {
    "id": 2018,
    "title": "BatMovel.jpg",
    "file_url": "https://vaiapi.s3.region.amazonaws.com/archive/2018/image/original.jpg"  
  }
]
```


## POST /v1/archives(.:format)

Create a new archive.

### URL Sample

```
/v1/archives.json
```

### URL Params

None

### Data Params

In the request URL, provide the following query parameters with values.

| Parameter | Type | Description | Required |
|:----------|:-----|:------------|:---------|
| title | String | Name of resource | true |
| author | String | Author of resource | false |
| description | String | Description of resource | true |
| published | Boolean | When true, resource will be visible | false |
| image | String PATH | Image path to upload | false |

### Example

##### Request

```
  curl --form archive[image]=@/marvin.jpeg \
      --form archive[title]='Sad Robot' \
      --form archive[author]='Marvin' \
      --form archive[description]='Lorem Ipsum' \
      http://localhost:3000/v1/archives
```

##### Response

```json
{
  "id": 18,
  "title": "A sad robot",
  "author": "Marvin",
  "description": "Lorem Ipsum",
  "published": true,
  "metadata": {
    "size": 17817,
    "width": 591,
    "height": 188,
    "filename": "marvin.jpg",
    "mime_type": "image/jpeg",
    "resolution": [ 72, 72 ],
    "uploaded_at": "2018-09-21T19:17:46.340Z"
  },
  "created_at": "2018-09-21T19:17:46.324Z",
  "file_url": "https://development-vaiapi.s3.sa-east-1.amazonaws.com/archive/18/image/original.jpg"
}
```

## GET /v1/archives/:id(.:format)

Fetch one single archive given id.

### URL Sample

```
/v1/archives/1.json
```

### URL Params

In the request URL, provide the following query parameters with values.

| Parameter | Type | Description |
|:----------|:-----|:------------|
| ID | Integer | Unique identifier

### Data Params

None

### Example

##### Request

```
  curl -X GET 'http://localhost:3000/v1/archives/1.json' 
```

##### Response

```json
{
  "id": 18,
  "title": "A sad robot",
  "author": "Marvin",
  "description": "Lorem Ipsum",
  "published": true,
  "metadata": {
    "size": 17817,
    "width": 591,
    "height": 188,
    "filename": "marvin.jpg",
    "mime_type": "image/jpeg",
    "resolution": [ 72, 72 ],
    "uploaded_at": "2018-09-21T19:17:46.340Z"
  },
  "created_at": "2018-09-21T19:17:46.324Z",
  "file_url": "https://development-vaiapi.s3.sa-east-1.amazonaws.com/archive/18/image/original.jpg"
}
```


## PUT /v1/archives/:id(.:format)

Updates a archive.

### URL Sample

```
/v1/archives/1.json
```

### URL Params

| Parameter | Type | Description |
|:----------|:-----|:------------|
| ID | Integer | Unique identifier

### Data Params

In the request URL, provide the following query parameters with values.

| Parameter | Type | Description | Required |
|:----------|:-----|:------------|:---------|
| title | String | Name of resource | true |
| author | String | Author of resource | false |
| description | String | Description of resource | true |
| published | Boolean | When true, resource will be visible | false |
| image | String PATH | Image path to upload | false |

### Example

##### Request

```
  curl -X PUT -d archive[published]=false http://localhost:3000/v1/archives/18.json
```

##### Response

```json
{
  "id": 18,
  "title": "A sad robot",
  "author": "Marvin",
  "description": "Lorem Ipsum",
  "published": false,
  "metadata": {
    "size": 17817,
    "width": 591,
    "height": 188,
    "filename": "marvin.jpg",
    "mime_type": "image/jpeg",
    "resolution": [ 72, 72 ],
    "uploaded_at": "2018-09-21T19:17:46.340Z"
  },
  "created_at": "2018-09-21T19:17:46.324Z",
  "file_url": "https://development-vaiapi.s3.sa-east-1.amazonaws.com/archive/18/image/original.jpg"
}
```


## DELETE /v1/archives/:id(.:format)

Destroy a archive.

### URL Sample

```
/v1/archives/1.json
```

### URL Params

| Parameter | Type | Description |
|:----------|:-----|:------------|
| ID | Integer | Unique identifier

### Data Params

None

### Example

##### Request

```
  curl -X DELETE http://localhost:3000/v1/archives/18.json
```

##### Response

None