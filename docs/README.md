# Vai API

A simple image uploading app.

### Resources List

The following tasks are available for Archive resources.

| Resource | Description |
|:---------|:------------|
| [Archive](Archive.md) | Represents images within the system |

### Description

No authentication required.