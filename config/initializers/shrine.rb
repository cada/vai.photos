require "shrine" # core
require "shrine/storage/s3"

s3_config = {
  access_key_id:     ENV["VAI_AWS_ACCESS_KEY_ID"],
  secret_access_key: ENV["VAI_AWS_SECRET_ACCESS_KEY"],
  bucket:            "#{Rails.env}-vaiapi",
  region:            "sa-east-1",
}

Shrine.storages = {
  cache: Shrine::Storage::S3.new(prefix: "cache", **s3_config),
  store: Shrine::Storage::S3.new(**s3_config),
}

Shrine.plugin :activerecord # enable ActiveRecord support
Shrine.plugin :cached_attachment_data # for forms
Shrine.plugin :restore_cached_data # refresh metadata when attaching the cached file
Shrine.plugin :determine_mime_type

# curl --form archive[image]=@/home/claudia/Documents/resume_sample.jpeg \
#      --form archive[title]=Melvin \
#      --form archive[description]=Oremos \
#      http://localhost:3000/v1/archives