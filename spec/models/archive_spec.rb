require 'rails_helper'

RSpec.describe Archive, type: :model do
  subject { FactoryBot.create(:archive) }
  it { expect(subject).to be_valid }

  describe 'metadata field should be a Hash' do
    it { expect(subject.metadata).to_not be_nil }
    it { expect(subject.metadata).to be_instance_of(Hash) }
  end

  describe '#file_url' do
    it { expect(subject).to respond_to(:file_url) }
  end
end
