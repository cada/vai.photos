include ActionDispatch::TestProcess
FactoryBot.define do
  factory :archive do
    title { "First Evidance" }
    description { "Found on crime local" }
    published { false }
    author { "Mr. Watson" }
    image { fixture_file_upload 'spec/fixtures/assets/images/car.png', 'image/png' }
    metadata { { author: 'Mr. Holmes', filename: 'proof_1.png' } }
  end
end
