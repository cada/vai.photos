class AddImageToArchive < ActiveRecord::Migration[5.2]
  def change
    add_column :archives, :image_data, :text
  end
end
