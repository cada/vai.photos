class CreateArchives < ActiveRecord::Migration[5.2]
  def change
    create_table :archives do |t|
      t.string :title
      t.string :author
      t.string :description
      t.boolean :published, default: true
      t.jsonb :metadata

      t.timestamps
    end
  end
end
