class V1::ArchivesController < V1::ApiController #ApplicationController
  before_action :set_archive, only: [:show, :update]

  # GET /archives
  def index
    @archives = Archive.published

    render json: @archives.as_json(only: [:id, :title], methods: [:file_url])
  end

  # GET /archives/1
  def show
    render json: @archive
  end

  # POST /archives
  def create
    @archive = Archive.new(archive_params)

    if @archive.save
      render json: @archive, status: :created, location: [:v1, @archive]
    else
      render json: @archive.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /archives/1
  def update
    if @archive.update(archive_params)
      render json: @archive
    else
      render json: @archive.errors, status: :unprocessable_entity
    end
  end

  # DELETE /archives/1
  def destroy
    @archive = Archive.find(params[:id])
    @archive.destroy
  end

  private
  
    # Use callbacks to share common setup or constraints between actions.
    def set_archive
      @archive = Archive.published.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def archive_params
      params.fetch(:archive, {}).permit(:title, :description, :author, :image, :published)
    end

end
