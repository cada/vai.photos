class Archive < ApplicationRecord
  include ImageUploader::Attachment.new(:image)

  validates :image_data, presence: true

  after_save :store_metadata
  after_save :set_title

  scope :published, -> { where(published: true) }

  def as_json(options = {})
    super({ except: [:image_data, :updated_at], methods: [:file_url] }.merge(options))
  end

  def file_url
    self.image[:original].try(:url) if (self.image && self.image.instance_of?(Hash))
  end

  private

    def set_title
      unless self.title
        self.title = self.image.metadata["filename"]
      end
    end

    def store_metadata
      if self.image
        self.metadata = self.image.try(:metadata) || self.image[:original].try(:metadata) || {}
        self.metadata[:uploaded_at] = Time.zone.now
      end
    end

end
