## Vai.File API

A simple file storage.

Our current environment uses the following versions:
* Rails 5.1 
* Ruby 2.4 

* System dependencies

Database powered by **Postgres** version 10.5 

... 

#### REST API Documentation

You can found adn read our API documentation on [docs](docs) or in our [Wiki](wikis/home)

#### Configuration

* Database creation: Run follow command:
 
`bundle exec rake db:create db:migrate`

* Database initialization

#### How to run the test suite 

Create the test database: 

`bundle exec rake db:create rake db:migrate RAILS_ENV=test` 

Run the test suite:

`bundle exec rspec` 


#### Services (job queues, cache servers, search engines, etc.)

... 

#### Deployment instructions

Hosted by Heroku. Simple deploy for contributors:

`git push heroku master`

Avaliable on https://vai-photos-api.herokuapp.com/v1/archives.json

... 

### Challenge Description

To code an image uploading service.

Requirements:
- Must upload an image and know (at least) the image’s upload date, a description, and the owner of the image.
- Description and Owner attributes can be free flowing text fields.
- Metadata should be stored in a database.
- Image "files" should not be stored in the Database.
- Upload should respond with a unique URL of a page that will show the image and associated metadata
- This process should be repeatable (i.e., multiple images should be uploadable, each with own metadata and each with it's own unique URL)

Bonus points for (optional): 
- Hosting a working copy of your app on Heroku
- Including tests/specs with your submission
